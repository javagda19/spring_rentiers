package com.javagda19.spring_rentiers.controller;

import com.javagda19.spring_rentiers.model.Apartment;
import com.javagda19.spring_rentiers.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/apartment/")
public class ApartmentController {

    @Autowired
    private ApartmentService apartmentService;

    @GetMapping(path = "/list")
    public String listApartments(Model model) {
        model.addAttribute("apartmentList", apartmentService.getAllApartments());
        return "apartmentList";
    }

    @GetMapping(path = "/details")
    public String apartmentDetails(@RequestParam(name = "apartmentId") Long id, Model model) {
        Optional<Apartment> apartmentOptional = apartmentService.getApartmentById(id);
        if (apartmentOptional.isPresent()) {
            // mamy mieszkanie, obsługa i wyświetlenie
            model.addAttribute("apartment", apartmentOptional.get());
            return "apartmentDetails";
        }
        // nie ma mieszkania, cóż zrobić?
        return "redirect:/apartment/list";
    }

    @GetMapping(path = "/delete")
    public String deleteApartment(@RequestParam(name = "apartmentId") Long id) {
        apartmentService.removeApartmentById(id);
        return "redirect:/apartment/list";
    }

    //################################ UPDATE
    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "apartmentId") Long id, Model model) {
        Optional<Apartment> apartmentOptional = apartmentService.getApartmentById(id);
        if (apartmentOptional.isPresent()) {
            // mamy mieszkanie, obsługa i wyświetlenie
            model.addAttribute("apartment", apartmentOptional.get());
            return "apartmentForm";
        }
        return "redirect:/apartment/list";
    }
//    ################################ UPDATE


    //    ################################ SAVE
    // SAVE DZIAŁA PRZY ZAPISIE/DODANIU ORAZ ZAPISIE/UPDATE
    @PostMapping(path = "/save")
    public String updateApartment(Model model, @Valid Apartment apartment) {
        try {
            apartmentService.update(apartment);
        } catch (Exception e) {
            model.addAttribute("apartment", apartment);
            return "apartmentForm";
        }
        return "redirect:/apartment/list";
    }
    //    ################################ SAVE


    //################################ ADD
    @GetMapping(path = "/add")
    public String apartmentForm(Model model) {
        model.addAttribute("apartment", new Apartment());

        return "apartmentForm";
    }
    //################################ UPDATE
}
