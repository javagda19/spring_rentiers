package com.javagda19.spring_rentiers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class SpringRentiersApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRentiersApplication.class, args);
    }

}
